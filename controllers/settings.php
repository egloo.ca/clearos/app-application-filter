<?php

/**
 * Application filter settings controller.
 *
 * @category   apps
 * @package    application-filter
 * @subpackage controllers
 * @author     eGloo <team@egloo.ca>
 * @copyright  2016 eGloo
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       https://www.netify.ai/resources/platforms/clearos
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * Application filter settings controller.
 *
 * @category   apps
 * @package    application-filter
 * @subpackage controllers
 * @author     eGloo <team@egloo.ca>
 * @copyright  2016 eGloo
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       https://www.netify.ai/resources/platforms/clearos
 */

class Settings extends ClearOS_Controller
{
    /**
     * Index view.
     *
     * @return view
     */

    function index()
    {
        $this->_common('index');
    }

    /**
     * Edit view.
     *
     * @return view
     */

    function edit()
    {
        $this->_common('edit');
    }

    /**
     * Common widget.
     *
     * @param string $type type
     *
     * @return view
     */

    function _common($type)
    {
        // Load libraries
        //---------------

        $this->lang->load('application_filter');
        $this->load->library('application_filter/Application_Filter');

        // Handle form submit
        //-------------------

        if ($this->input->post('submit')) {
            try {
                $rules = [];
                $encoded_rules = $this->input->post('rules'); 

                foreach ($encoded_rules as $name => $value)
                    $rules[base64_decode(strtr($name, '-_:', '+/='))] = $value;

                $this->application_filter->set_rules($rules);
                $this->application_filter->update_state();

                $this->page->set_status_updated();
            } catch (Exception $e) {
                $this->page->view_exception($e);
                return;
            }
        }

        // Load view data
        //---------------

        try {
            $data['rules'] = $this->application_filter->get_rules();
        } catch (Exception $e) {
            $this->page->view_exception($e);
            return;
        }

        $form = ($type == 'index') ? 'summary' : 'settings'; 

        // Load views
        //-----------

        $this->page->view_form($form, $data, lang('base_settings'));
    }
}
