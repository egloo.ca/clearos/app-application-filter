<?php

/**
 * Application filter settings view.
 *
 * @category   apps
 * @package    application-filter
 * @subpackage views
 * @author     eGloo <team@egloo.ca>
 * @copyright  2016 eGloo
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       https://www.netify.ai/resources/platforms/clearos
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.  
//  
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Load dependencies
///////////////////////////////////////////////////////////////////////////////

$this->lang->load('base');
$this->lang->load('netify');
$this->lang->load('application_filter');

///////////////////////////////////////////////////////////////////////////////
// Buttons
///////////////////////////////////////////////////////////////////////////////

$buttons = array(
    anchor_cancel('/app/application_filter/settings'),
    form_submit_update('submit', 'high')
);

///////////////////////////////////////////////////////////////////////////////
// Headers
///////////////////////////////////////////////////////////////////////////////

$headers = array(
    lang('netify_category'),
    lang('netify_application'),
);

///////////////////////////////////////////////////////////////////////////////
// Items
///////////////////////////////////////////////////////////////////////////////

foreach ($rules as $id => $entry) {
    if (empty($entry['url']))
        $name = $entry['name'];
    else
        $name = "<i class='fa fa-external-link theme-text-icon-spacing'></i> <a target='_blank' href='" . $entry['url'] . "'>" . $entry['name'] . "</a>";

    // Tags might contain invalid characters, so encode
    $id = strtr(base64_encode($id), '+/=', '-_:'); ;

    // TODO: integrate ad-hoc icon into theme
    $item['title'] = $entry['name'];
    $item['name'] = 'rules[' . $id . ']';
    $item['state'] = empty($entry['state']) ? FALSE : $entry['state'];
    $item['details'] = array(
        $entry['category'],
        $name,
    );

    $items[] = $item;
}

sort($items);

///////////////////////////////////////////////////////////////////////////////
// List table
///////////////////////////////////////////////////////////////////////////////

$options['grouping'] = TRUE;
$options['id'] = 'application_edit';

echo "<div id='application_edit_wrapper' class='hide'>";

echo form_open('application_filter');

echo list_table(
    lang('netify_block_applications'),
    $buttons,
    $headers,
    $items,
    $options
);

echo form_close();

echo "</div>";
