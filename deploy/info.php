<?php

/////////////////////////////////////////////////////////////////////////////
// General information
/////////////////////////////////////////////////////////////////////////////

$app['basename'] = 'application_filter';
$app['version'] = '2.3.3';
$app['vendor'] = 'eGloo';
$app['packager'] = 'eGloo';
$app['license'] = 'GPLv3';
$app['license_core'] = 'LGPLv3';
$app['description'] = lang('application_filter_app_description');

/////////////////////////////////////////////////////////////////////////////
// App name and categories
/////////////////////////////////////////////////////////////////////////////

$app['name'] = lang('application_filter_app_name');
$app['category'] = lang('base_category_gateway');
$app['subcategory'] = 'Filtering';

/////////////////////////////////////////////////////////////////////////////
// Controllers
/////////////////////////////////////////////////////////////////////////////

$app['controllers']['application_filter']['title'] = $app['name'];
$app['controllers']['settings']['title'] = lang('base_settings');
$app['controllers']['white_list']['title'] = lang('netify_white_list');

/////////////////////////////////////////////////////////////////////////////
// Packaging
/////////////////////////////////////////////////////////////////////////////

// FIXME: remove theme version requirement in ClearOS v8
$app['requires'] = array(
    'app-network',
    'theme-clearos-admin >= 7.1.18'
);

$app['core_requires'] = array(
    'app-firewall-core',
    'app-network-core',
    'app-netify-core >= 1:2.3.0',
    'app-netify-fwa-core >= 1:2.3.0',
);

$app['delete_dependency'] = array(
    'app-application-filter-core',
);
