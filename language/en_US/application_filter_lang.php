<?php

$lang['application_filter_app_name'] = 'Application Filter';
$lang['application_filter_app_description'] = 'Take control of unwanted traffic on your network.  The Application Filter can detect and block apps like Facebook, Netflix, Snapchat, and many others.';
